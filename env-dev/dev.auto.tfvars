env = "dev"

vcmais_network = {
  vpc_cidr          = "10.52.0.0/16"
}

vcmais_vpc_subnets = ["10.52.1.0/24", "10.52.2.0/24"]

vcmais_vpc_private_subnets = ["10.52.3.0/24", "10.52.4.0/24"]

main_cluster_azs = ["us-east-1a", "us-east-1b"]