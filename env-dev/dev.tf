provider "aws" {
  region  = "us-east-1"
  version = "2.14.0"
}

# Global variables

terraform {
  backend "s3" {
    bucket = "vcmais-terraform"
    key    = "tf-state/vcmais-dev.tfstate"
    region = "us-east-1"
  }
}

module "core" {
  source = "../modules/core"

  env                        = "${var.env}"
  vcmais_network             = "${var.vcmais_network}"
  main_cluster_azs           = "${var.main_cluster_azs}"
  vcmais_vpc_subnets         = "${var.vcmais_vpc_subnets}"
  vcmais_vpc_private_subnets = "${var.vcmais_vpc_private_subnets}"
}
