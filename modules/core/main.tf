module "network" {
  source = "../network"

  env                       = "${var.env}"
  vcmais_network            = "${var.vcmais_network}"
  azs                       = "${var.main_cluster_azs}"
  subnet_cidr_block         = "${var.vcmais_vpc_subnets}"
  private_subnet_cidr_block = "${var.vcmais_vpc_private_subnets}"

  tags = "${local.tags_defaults}"
}
