locals {
  tags_defaults = {
    Terraform = "true"
  }
}

variable "env" {
  type = "string"
}

variable "vcmais_network" {
  type = "map"

  default = {}

  description = <<EOF
  vpc_cidr          = (Optional) "Vcmais vpc CIDR block"
  EOF
}

variable "vcmais_vpc_subnets" {
  type        = "list"
  description = "CIDR for overall vcmais VPC Subnets"
}

variable "vcmais_vpc_private_subnets" {
  type        = "list"
  description = "CIDR for private vcmais VPC Subnets"
}

variable "main_cluster_azs" {
  type        = "list"
  description = "Availability zones for individual subnets"
}
