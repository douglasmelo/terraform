resource "aws_internet_gateway" "core" {
  vpc_id = "${aws_vpc.vcmais_vpc.id}"

  tags = "${merge(var.tags, map("Name", format("%s-%s", var.name, var.env)))}"
}