resource "aws_vpc" "vcmais_vpc" {
  cidr_block = "${lookup(var.vcmais_network, "vpc_cidr")}"

  enable_dns_support   = "true"
  enable_dns_hostnames = "true"

  tags = "${merge(var.tags, map("Name", "vcmais" ))}"
}
