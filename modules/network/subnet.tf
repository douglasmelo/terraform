resource "aws_subnet" "subnet_public" {
  count = "${length(var.subnet_cidr_block)}"

  vpc_id                  = "${aws_vpc.vcmais_vpc.id}"
  cidr_block              = "${var.subnet_cidr_block[count.index]}"
  availability_zone       = "${var.azs[count.index]}"
  map_public_ip_on_launch = "${var.core_public_ip_on_launch}"

  tags = "${merge(var.tags, map("Tier", "public", "Name", format("%s-%s.Public-%d", var.name, var.env, count.index + 1)))}"
}

resource "aws_subnet" "subnet_private" {
  count = "${length(var.private_subnet_cidr_block)}"

  vpc_id            = "${aws_vpc.vcmais_vpc.id}"
  cidr_block        = "${var.private_subnet_cidr_block[count.index]}"
  availability_zone = "${var.azs[count.index]}"

  map_public_ip_on_launch = "false"

  tags = "${merge(var.tags, map("Tier", "private", "Name", format("%s-%s.Private-%d", var.name, var.env, count.index + 1)))}"
}
