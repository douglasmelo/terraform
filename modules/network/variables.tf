variable "vcmais_network" {
  type = "map"

  default = {}

  description = <<EOF
  subnet_cidr_block = (Optional) "Vcmais subnet CIDR block"
  vpc_cidr     = (Optional) "Vcmais vpc CIDR block"
  EOF
}

variable "tags" {
  type = "map"

  default = {}

  description = <<EOF
    Name                  = (Optional) "AWS Naming."
    Terraform             = (Optional) "Determine if resource is controlled by Terraform."
  EOF
}

variable "subnet_cidr_block" {
  type = "list"
}

variable "azs" {
  type = "list"
}

variable "core_public_ip_on_launch" {
  type        = "string"
  default     = "false"
  description = "Specify true to indicate that instances launched into the subnet should be assigned a public IP address"
}

variable "private_subnet_cidr_block" {
  type    = "list"
  default = []
}

variable "env" {
  type = "string"
}

variable "name" {
  default = "Vcmais"
}
