resource "aws_nat_gateway" "nat_vcmais" {
  count = "${length(var.private_subnet_cidr_block) > 0 ? length(var.subnet_cidr_block) : 0}"

  allocation_id = "${element(aws_eip.eip_vcmais.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.subnet_public.*.id, count.index)}"

  tags = "${merge(var.tags, map( "Name", format("%s-%s.nat-%d", var.name, var.env, count.index + 1)))}"
}

resource "aws_eip" "eip_vcmais" {
  count = "${length(var.private_subnet_cidr_block) > 0 ? length(var.subnet_cidr_block) : 0}"

  vpc = "true"

  tags = "${merge(var.tags, map( "Name", format("%s-%s.eip-%d", var.name, var.env, count.index + 1)))}"
}
