The backend of this project is configured to be stored in S3, the first step need to be executed manually.

Create a bucket named as `vcmais-terraform` then this structure will be saved inside it. 

`tf-state/vcmais-dev.tfstate`

Enter in your env directory. Eg. env-dev.


Since you are inside terraform/env-dev, you can initialize the terraform execute the following command:

`terraform init`

You need to execute this command every time you change modules files or provider.


After you have initialize your terraform you can execute the plan to see if there is any pending modification to apply.

`terraform plan`


If you are comfortable with your changes you can apply executing the following command:

`terraform apply`